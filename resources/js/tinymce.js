import tinymce from 'tinymce/tinymce';
window.tinymce = tinymce;

// A theme is also required
import 'tinymce/themes/silver';

// Any plugins you want to use has to be imported
import 'tinymce/plugins/advlist';
import 'tinymce/plugins/autolink';
import 'tinymce/plugins/lists';
import 'tinymce/plugins/link';
import 'tinymce/plugins/image';
import 'tinymce/plugins/charmap';
import 'tinymce/plugins/print';
import 'tinymce/plugins/preview';
import 'tinymce/plugins/hr';
import 'tinymce/plugins/anchor';
import 'tinymce/plugins/pagebreak';
import 'tinymce/plugins/searchreplace';
import 'tinymce/plugins/wordcount';
import 'tinymce/plugins/visualblocks';
import 'tinymce/plugins/visualchars';
import 'tinymce/plugins/code';
import 'tinymce/plugins/fullscreen';
import 'tinymce/plugins/insertdatetime';
import 'tinymce/plugins/media';
import 'tinymce/plugins/nonbreaking';
import 'tinymce/plugins/save';
import 'tinymce/plugins/table';
import 'tinymce/plugins/directionality';
import 'tinymce/plugins/emoticons';
import 'tinymce/plugins/emoticons/js/emojis';
import 'tinymce/plugins/template';
import 'tinymce/plugins/paste';
import 'tinymce/plugins/textcolor';
import 'tinymce/plugins/textpattern';

import 'tinymce/plugins/toc';
import 'tinymce/plugins/tabfocus';
import 'tinymce/plugins/spellchecker';
import 'tinymce/plugins/quickbars';
import 'tinymce/plugins/noneditable';
import 'tinymce/plugins/legacyoutput';
import 'tinymce/plugins/importcss';
import 'tinymce/plugins/imagetools';
import 'tinymce/plugins/help';
import 'tinymce/plugins/fullpage';
import 'tinymce/plugins/contextmenu';
import 'tinymce/plugins/colorpicker';
import 'tinymce/plugins/codesample';
import 'tinymce/plugins/bbcode';
import 'tinymce/plugins/autosave';
import 'tinymce/plugins/autoresize';
